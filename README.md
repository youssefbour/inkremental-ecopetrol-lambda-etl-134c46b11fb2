# Ecopetrol lambda etl

## Before start

1. Install python 3.8
2. Install requirements in requirements.txt

```bash
pip3 install -r requirements.txt
```

3. Enjoy it

---

## Run local

Run with python console (and debug)

#### DEV

```python
import os

os.environ["STAGE"] = "dev"
os.environ["DEBUG"] = "True"
from handlers.example import hello

hello(None, None)
```

#### Staging

```python
import os

os.environ["STAGE"] = "dev"
os.environ["DEBUG"] = "True"
from handlers.example import hello

hello(None, None)
```

#### Production

```python
import os

os.environ["STAGE"] = "dev"
os.environ["DEBUG"] = "True"
from handlers.example import hello

hello(None, None)
```

---

## Create new function

1. Create new file inside folder called "handlers".
2. Code new function there, remember that all files needs to have this conventions

```python
try:
    import unzip_requirements
except ImportError:
    pass
import logging
import settings
import timeit
import json

# ...
# Add all imports here
# ...

logger = logging.getLogger()
logger.setLevel(settings.get_logger_level())


# ...
# Constants here
# ...

# Events here
def lambda_function(event, context):
    logger.info("lambda_function... %s" % json.dumps(event))
    start = timeit.default_timer()
    # Write lambda code here
    # ...
    logger.debug("This is an example function")
    # ...
    logger.info("lambda_function returned in %s" % (timeit.default_timer() - start))
    return

# ...
# Other methods here
# ...

```

3. Test function local.
4. Add function into serverless.yml file.

---

## Deploy functions

Dev

```
serverless deploy --stage dev --aws-profile serverless_staging
```

Staging

```
serverless deploy --stage staging --aws-profile serverless_staging
```

Production

```
serverless deploy --stage production --aws-profile serverless_prod
```

